import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EstadostareascrudComponent } from './estadostareascrud/estadostareascrud.component';
import { WelcomeComponent } from './welcome/welcome.component';
/*import { PhoneDetailComponent } from './phone-detail/phone-detail.component';*/

const routes: Routes = [
  { path: 'estadostareas', component: EstadostareascrudComponent },
  /*{ path: 'phones/:name', component: PhoneDetailComponent },*/
  { path: 'welcome', component: WelcomeComponent },
  { path: '', component: WelcomeComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}