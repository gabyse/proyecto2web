import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { EstadostareasService } from './estadostareas.service';


import { AppComponent } from './app.component';
import { EstadostareascrudComponent } from './estadostareascrud/estadostareascrud.component';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    EstadostareascrudComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [EstadostareasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
