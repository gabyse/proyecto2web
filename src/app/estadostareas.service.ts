import { Injectable } from '@angular/core';
import { Estadostareas } from './estadostareas';

@Injectable()
export class EstadostareasService {
  data: Estadostareas[];
  constructor() {
  	this.data = JSON.parse(localStorage.getItem('estadostareas') || '[]');
   }

   read() {
    this.data = JSON.parse(localStorage.getItem('estadostareas') || '[]');
    return this.data;
  }

  save(data: Estadostareas[]) {
    this.data = data;
    localStorage.setItem('estadostareas', JSON.stringify(this.data));
  }

  findByName(id: string) {
    return this.data.find(x => x.id === id);
  }

}
