import { TestBed, inject } from '@angular/core/testing';

import { EstadostareasService } from './estadostareas.service';

describe('EstadostareasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstadostareasService]
    });
  });

  it('should be created', inject([EstadostareasService], (service: EstadostareasService) => {
    expect(service).toBeTruthy();
  }));
});
