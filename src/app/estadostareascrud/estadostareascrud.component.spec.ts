import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadostareascrudComponent } from './estadostareascrud.component';

describe('EstadostareascrudComponent', () => {
  let component: EstadostareascrudComponent;
  let fixture: ComponentFixture<EstadostareascrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadostareascrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadostareascrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
