import { Component, OnInit } from '@angular/core';
import { Estadostareas } from '../estadostareas';
import { EstadostareasService } from '../estadostareas.service';

@Component({
  selector: 'app-estadostareascrud',
  templateUrl: './estadostareascrud.component.html',
  styleUrls: ['./estadostareascrud.component.css']
})
export class EstadostareascrudComponent implements OnInit {

  data: Estadostareas[];
  current_estadostareas: Estadostareas;
  crud_operation = { is_new: false, is_visible: false };
  constructor(private service: EstadostareasService) { }

  ngOnInit() {
  	this.data = JSON.parse(localStorage.getItem('estadostareas') || '[]');
    this.current_estadostareas = new Estadostareas();
  }

  new() {
    this.current_estadostareas = new Estadostareas();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  edit(row) {
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_estadostareas = row;
  }

  delete(row) {
    this.crud_operation.is_new = false;
    const index = this.data.indexOf(row, 0);
    if (index > -1) {
      this.data.splice(index, 1);
    }
    this.save();
  }

  save() {
    if (this.crud_operation.is_new) {
      this.data.push(this.current_estadostareas);
    }
    this.service.save(this.data);
    this.current_estadostareas = new Estadostareas();
    this.crud_operation.is_visible = false;
  }
}
